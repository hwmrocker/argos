import re
import asyncio
from aiohttp import web

re_time = re.compile(r'time=(\d+(?:\.\d+)?) (\w+)')


@asyncio.coroutine
def get_icmp(target):
    ret_str = "probe_duration_seconds {:0.6f}\nprobe_success {}\n"

    if ":" in target:
        target = target[:target.find(':')]
    coro = asyncio.create_subprocess_shell("ping -c1 -w2 " + target,
            stdout=asyncio.subprocess.PIPE)
    proc = yield from coro
    yield from proc.wait()
    data = yield from proc.stdout.read()
    data = data.decode()
    if proc.returncode != 0:
        try:
            print("{} down ..rc={}".format(target, proc.returncode))
            print(data)
        except Exception:
            pass
        finally:
            return ret_str.format(5, 0)
    time, unit = re_time.findall(data)[0]
    time = float(time)
    if unit == "ms":
        time /= 1000.
    return ret_str.format(time, 1)


@asyncio.coroutine
def handle(request):
    name = request.match_info.get('name', "Anonymous")
    module = request.GET.get('module', 'icmp')
    target = request.GET.get('target', None)
    # print(request.path_qs)
    if module == "icmp" and target:
        # print(repr(target))
        text = yield from get_icmp(target)
        # print(text)
    else:
        text = ""
    return web.Response(body=text.encode('utf-8'))


@asyncio.coroutine
def init(loop):
    app = web.Application(loop=loop)
    app.router.add_route('GET', '/{name:.*}', handle)

    srv = yield from loop.create_server(app.make_handler(), '127.0.0.1', 8081)
    print("Server started at http://127.0.0.1:8081")
    return srv

loop = asyncio.get_event_loop()
loop.run_until_complete(init(loop))
loop.run_forever()
