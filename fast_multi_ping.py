#!/usr/bin/env python3
import re
import asyncio

# re_time = re.compile(r'time=(\d+(?:\.\d+)?) (\w+)')

CHECK = ['eltanin.uberspace.de',
 'elnath.uberspace.de',
 'mirfac.uberspace.de',
 'dubhe.uberspace.de',
 'kaus.uberspace.de',
 'hadar.uberspace.de',
 'alkaid.uberspace.de',
 # 'leo.uberspace.de',
 'grus.uberspace.de',
 'draco.uberspace.de',
 'bellatrix.uberspace.de',
 'bootes.uberspace.de',
 'sculptor.uberspace.de',
 'cygnus.uberspace.de',
 'canis.uberspace.de',
 'sagitta.uberspace.de',
 'antila.uberspace.de',
 'tucana.uberspace.de',
 'centaurus.uberspace.de',
 'canopus.uberspace.de',
 'lyra.uberspace.de',
 'corona.uberspace.de',
 'shaula.uberspace.de',
 'volans.uberspace.de',
 'orion.uberspace.de',
 'betelgeuse.uberspace.de',
 'delphinus.uberspace.de',
 'serpens.uberspace.de',
 'alphard.uberspace.de',
 'ursa.uberspace.de',
 'acamar.uberspace.de',
 'octans.uberspace.de',
 'hydrus.uberspace.de',
 'pisces.uberspace.de',
 'perseus.uberspace.de',
 'corvus.uberspace.de',
 'carina.uberspace.de',
 'columba.uberspace.de',
 'vulpecula.uberspace.de',
 'achernar.uberspace.de',
 'atria.uberspace.de',
 'scorpius.uberspace.de',
 'dorado.uberspace.de',
 'crux.uberspace.de',
 'fomalhaut.uberspace.de',
 'peacock.uberspace.de',
 'acrux.uberspace.de',
 'triangulum.uberspace.de',
 'capella.uberspace.de',
 'auriga.uberspace.de',
 'hamal.uberspace.de',
 'spica.uberspace.de',
 'aldebaran.uberspace.de',
 'alpheca.uberspace.de',
 'hercules.uberspace.de',
 'avior.uberspace.de',
 'libra.uberspace.de',
 'sabic.uberspace.de',
 'pyxis.uberspace.de',
 'cassiopeia.uberspace.de',
 'cepheus.uberspace.de',
 'markab.uberspace.de',
 'eridanus.uberspace.de',
 'ara.uberspace.de',
 'vela.uberspace.de',
 'horologium.uberspace.de',
 'fornax.uberspace.de',
 'alnilam.uberspace.de',
 'lynx.uberspace.de',
 'nunki.uberspace.de',
 'aquila.uberspace.de',
 'norma.uberspace.de',
 'andromeda.uberspace.de',
 'sirius.uberspace.de',
 'caelum.uberspace.de',
 'suhail.uberspace.de',
 'musca.uberspace.de',
 'hydra.uberspace.de',
 'menkar.uberspace.de',
 'apus.uberspace.de',
 'menkent.uberspace.de',
 'pegasus.uberspace.de',
 'mensa.uberspace.de',
 'lepus.uberspace.de',
 'alioth.uberspace.de',
 'lupus.uberspace.de',
 'adhara.uberspace.de',
 'regulus.uberspace.de',
 'phoenix.uberspace.de',
 'enif.uberspace.de',
 'diphda.uberspace.de',
 'kochab.uberspace.de',
 'cetus.uberspace.de',
 'circinus.uberspace.de',
 'arcturus.uberspace.de',
 'aries.uberspace.de',
 'vega.uberspace.de',
 'pavo.uberspace.de',
 'monoceres.uberspace.de',
 'crater.uberspace.de',
 'rigel.uberspace.de',
 'pictor.uberspace.de',
 'indus.uberspace.de',
 'puppis.uberspace.de',
 'schedar.uberspace.de',
 'virgo.uberspace.de',
 'deneb.uberspace.de',
 'lacerta.uberspace.de',
 'aquarius.uberspace.de',
 'gienah.uberspace.de',
 'antares.uberspace.de',
 'taurus.uberspace.de']

@asyncio.coroutine
def ping_foo(hostname):
    coro = asyncio.create_subprocess_shell("ping -c1 -w2 " + hostname,
            stdout=asyncio.subprocess.PIPE)
    proc = yield from coro
    data = yield from proc.stdout.read()
    # print("{} finished with {}".format(hostname, proc.returncode))
    return hostname, proc.returncode

@asyncio.coroutine
def ping_stats():
    coro_list = []
    proc_to_name = {}
    for hostname in CHECK:
        coro_list.append(ping_foo(hostname))

    pending = True
    while pending:
        done, pending = yield from asyncio.wait(coro_list)
        for finished in done:
            # get porcess and data from finished job
            # print(data.decode())
            # print()
            hostname, retcode = finished.result()
            print("{} finished with {}".format(hostname, retcode))



loop = asyncio.get_event_loop()
# asyncio.async(init(loop))
# asyncio.async(ping_stats())
loop.run_until_complete(ping_stats())
loop.close()
